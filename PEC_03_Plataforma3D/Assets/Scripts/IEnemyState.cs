using UnityEngine;
using UnityEngine.AI;

public interface IEnemyState
{

    void UpdateState();
    void GoToAttackState();
    void GoToAlertState();
    void GoToPatrolState();


    void OnTriggerEnter(Collider col);
    void OnTriggerStay(Collider col);
    void OnTriggerExit(Collider col);

    void Impact();
}




public class PatrolState : IEnemyState
{
    EnemyManager myEnemy;
    private Vector3 randomDestination;
    private bool hasRandomDestination;

    public float radius = 10;

    public PatrolState(EnemyManager enemy)
    {
       
        myEnemy = enemy;
    }

    public void UpdateState()
    {

        
        if (!hasRandomDestination && !myEnemy.isDead)
        {
            myEnemy.gameObject.transform.GetComponent<Animator>().SetInteger("StateEnemy", 0);
            // Si no hay un destino aleatorio actual, genera uno nuevo
            randomDestination = GetRandomPosition();
            hasRandomDestination = true;
            

        }

        if (myEnemy.navMeshAgent.remainingDistance <= myEnemy.navMeshAgent.stoppingDistance && !myEnemy.isDead)
        {
            myEnemy.gameObject.transform.GetComponent<Animator>().SetInteger("StateEnemy", 1);
            // Si el enemigo ha alcanzado su destino actual, establece el siguiente destino aleatorio
            randomDestination = GetRandomPosition();
            myEnemy.navMeshAgent.SetDestination(randomDestination);
            hasRandomDestination = false;
            
            

        }
        if (myEnemy.isDead)
        {
            myEnemy.gameObject.transform.GetComponent<Animator>().SetInteger("StateEnemy", 2);
            myEnemy.Stop();
        }
    }

    private Vector3 GetRandomPosition()
    {
        // Obtiene una posici�n aleatoria dentro de un rango alrededor del enemigo
        Vector3 randomPosition = Random.insideUnitSphere * radius;

        // Suma la posici�n aleatoria al origen del enemigo
        randomPosition += myEnemy.transform.position;

        // Asegura que la posici�n se encuentre en el suelo
        NavMeshHit navMeshHit;
        NavMesh.SamplePosition(randomPosition, out navMeshHit, radius, NavMesh.AllAreas);
        
        return navMeshHit.position;
    }


    public void Impact()
    {
        GoToAlertState();
    }

    public void GoToAlertState()
    {
        myEnemy.Stop();
        myEnemy.currentState = myEnemy.alertState;
    }

    public void GoToAttackState()
    {
        myEnemy.Stop();
        myEnemy.currentState = myEnemy.attackState;
    }

    public void GoToPatrolState() { }

    public void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
            GoToAlertState();
    }
    public void OnTriggerStay(Collider col)
    {
        if (col.gameObject.tag == "Player")
            GoToAlertState();
    }
    public void OnTriggerExit(Collider col) { }
}

public class AlertState : IEnemyState
{
    EnemyManager myEnemy;
    float currentRotationTime = 0;

    public AlertState(EnemyManager enemy)
    {
        myEnemy = enemy;
    }

    public void UpdateState()
    {
        if (!myEnemy.isDead)
        {
            myEnemy.transform.rotation *= Quaternion.Euler(0f, Time.deltaTime * 360 * 2.0f / myEnemy.rotationTime, 0f);
            Debug.Log("Estan en alerta");
            myEnemy.gameObject.transform.GetComponent<Animator>().SetInteger("StateEnemy", 0);

            if (currentRotationTime > myEnemy.rotationTime)
            {
                currentRotationTime = 0;
                GoToPatrolState();
                Debug.Log("Sigo caminando");
            }
            else
            {

                Vector3 raycastOrigin = myEnemy.transform.position + new Vector3(0f, 1f, 0f);
                Vector3 raycastDirection = myEnemy.transform.forward * 100f;
                Ray ray = new Ray(raycastOrigin, raycastDirection);
                Debug.DrawRay(ray.origin, raycastDirection, Color.red);

                if (Physics.Raycast(ray, out RaycastHit hit))
                {

                    if (hit.collider.gameObject.tag == "Player")
                    {
                        Debug.Log("Atacando al player");
                        Debug.Log(hit.collider.name);
                        GoToAttackState();
                    }
                }
            }
        }
        if (myEnemy.isDead)
        {
            myEnemy.gameObject.transform.GetComponent<Animator>().SetInteger("StateEnemy", 2);
            myEnemy.Stop();
        }
        currentRotationTime += Time.deltaTime;
    }

   

    public void Impact()
    {
        Debug.Log("Estas atacando al enemigo");
        GoToAttackState();
    }

    public void GoToAlertState() { }

    public void GoToAttackState()
    {
        myEnemy.currentState = myEnemy.attackState;
    }

    public void GoToPatrolState()
    {
        myEnemy.Resume();
        myEnemy.currentState = myEnemy.patrolState;
    }

    public void OnTriggerEnter(Collider col) { }
    public void OnTriggerStay(Collider col) { }
    public void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            GoToPatrolState();

        }
    }

}

public class AttackState : IEnemyState
{
    EnemyManager myEnemy;
    float actualTimeBetweenShoots = 0;

    //private bool playerDetected = false;
    public AttackState(EnemyManager enemy)
    {
        myEnemy = enemy;
    }

    public void UpdateState()
    {
        
        /*if (!playerDetected)
        {
            myEnemy.myLight.color = Color.red;
            actualTimeBetweenShoots += Time.deltaTime;
        }
        else
        {
            GoToPatrolState();
        }*/
        //myEnemy.myLight.color = Color.red;
        actualTimeBetweenShoots += Time.deltaTime;
    }

    

    public void Impact() { }

    public void GoToAttackState() { }
    public void GoToPatrolState()
    {
        myEnemy.Resume();
        myEnemy.currentState = myEnemy.patrolState;
    }

    public void GoToAlertState()
    {
        myEnemy.currentState = myEnemy.alertState;
    }

    public void OnTriggerEnter(Collider col) { }

    public void OnTriggerStay(Collider col)
    {
        if (!myEnemy.isDead)
        {
            Vector3 targetPosition = col.transform.position;
            Vector3 moveDirection = targetPosition - myEnemy.transform.position;
            moveDirection.Normalize();

            float moveSpeed = 0.25f; // Velocidad de movimiento del enemigo
            myEnemy.transform.position += moveDirection * moveSpeed * Time.deltaTime;
            myEnemy.gameObject.GetComponent<Animator>().SetTrigger("Attack");
            Debug.Log("Viene hacia mi el enemigo");
        }
            

        if (myEnemy.isDead)
        {
            myEnemy.gameObject.GetComponent<Animator>().SetTrigger("Dying");
            myEnemy.Stop();
        }

    }
    public void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            GoToPatrolState();
            myEnemy.gameObject.transform.GetComponent<Animator>().SetInteger("StateEnemy", 0);

        }

    }


}
