using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    public float maxHealth = 100f;
    public float health;

    public bool IsDead = false;
    public bool IsPaused = false;

    public GameObject bulletGun;
    public GameObject creatorBulletGun;
    public float forceGun;
    public int maxAmmoGun = 25; // n�mero m�ximo de balas que puede contener el cargador
    public int AmmoGun; // n�mero actual de balas en el cargador
    public int wearSword = 10;
    public int swordSolid;

    public UIManager uiManager;
    private EnemyManager enemyManager;

    private void Start()
    {
        health = maxHealth;
        AmmoGun = maxAmmoGun;
        swordSolid= wearSword;
        uiManager = GameObject.FindObjectOfType<UIManager>();
        enemyManager = GameObject.FindObjectOfType<EnemyManager>();
    }

    public void TakeDamage(float amount)
    {
        health -= amount;
        if (health <= 0)
        {
            Die();
        }
        uiManager.healthSlider.value = health;
        Debug.Log("Me estan golpeando");
    }

    public void Heal(int amount)
    {
        health += amount;
        if (health > maxHealth)
        {
            health = maxHealth;
        }
        uiManager.healthSlider.value = health;
        Debug.Log("Me he recuperado");
    }

    public void ReloadedAmmo(int ammo)
    {
        AmmoGun += ammo;
        if (AmmoGun > maxAmmoGun)
        {
            AmmoGun = maxAmmoGun;
        }
        UpdateAmmoTextGun();
        Debug.Log("He recargado");
    }

    public void UpdateSwordText()
    {
        uiManager.swordText.text = swordSolid.ToString();
    }

    public void UpdateAmmoTextGun()
    {
        uiManager.ammoTextGun.text = AmmoGun.ToString();
    }

    public void Die()
    {
        IsDead = true;
        this.gameObject.GetComponent<Animator>().SetTrigger("Dying");
        Debug.Log("Estoy muerto");
        uiManager.GameOver();
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        enemyManager.isWin = true;
    }

    public void Win()
    {
        Debug.Log("He ganado");
        uiManager.GameWin();
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    


}

