using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{
    //SCENE
    public GameObject gameOverImage;
    public GameObject gameWinImage;
    public GameObject gamePauseImage;

    public TextMeshProUGUI ammoTextGun;
    public TextMeshProUGUI swordText;
    public Slider healthSlider;

    public GameObject pointViewImageOne;
    


    // Start is called before the first frame update
    void Start()
    {
        gameOverImage.SetActive(false);
        gameWinImage.SetActive(false);
        gamePauseImage.SetActive(false);
        pointViewImageOne.SetActive(false);
        
    }


    //SCENE
    public void AgainLevel()
    {
        SceneManager.LoadScene("Prototip");
    }

    public void ExitToMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    public void GameOver()
    {
        gameOverImage.SetActive(true);
    }

    public void GameWin()
    {
        gameWinImage.SetActive(true);
    }

    public void GamePause()
    {
        gamePauseImage.SetActive(true);
    }

    public void NoGamePause()
    {
        gamePauseImage.SetActive(false);
    }



    

}
