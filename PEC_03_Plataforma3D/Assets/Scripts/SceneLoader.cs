using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    //MENU
    //public GameObject menuButtonImage;
    public GameObject playButtonImage;
    //public GameObject settingsButtonImage;
    public GameObject creditsButtonImage;
    public GameObject exitButtonImage;

    public GameObject creditsImage;

    private void Start()
    {
        playButtonImage.SetActive(false);
        //settingsButtonImage.SetActive(false);
        creditsButtonImage.SetActive(false);
        exitButtonImage.SetActive(false);
        creditsImage.SetActive(false);

    }


    public void NextLevel()
    {
        SceneManager.LoadScene("Prototip");
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void LoadCredits()
    {
        creditsImage.SetActive(true);
        creditsButtonImage.SetActive(false);
    }

    //Play
    public void Play()
    {
        playButtonImage.SetActive(true);
    }
    public void NoPlay()
    {
        playButtonImage.SetActive(false);
    }

    //Exit
    public void Exit()
    {
        exitButtonImage.SetActive(true);
    }
    public void NoExit()
    {
        exitButtonImage.SetActive(false);
    }

    //Credits
    public void Credits()
    {
        creditsButtonImage.SetActive(true);
    }
    public void NoCredits()
    {
        creditsButtonImage.SetActive(false);
    }

    public void BackMenuFromCredtis()
    {
        creditsImage.SetActive(false);
    }



}
