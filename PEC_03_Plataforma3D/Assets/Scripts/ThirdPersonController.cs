using Unity.VisualScripting;
using UnityEngine;

public class ThirdPersonController : MonoBehaviour
{
    [SerializeField] private float walkSpeed = 5f;
    [SerializeField] private float runSpeed = 30f;
    [SerializeField] private float jumpSpeed = 10f;
    [SerializeField] private float stickToGroundForce = 10f;
    [SerializeField] private float gravittyMultiplier = 2f;
    [SerializeField] private float m_MouseSensitivity = 2.0f;

    private bool isWalking;
    private bool jump;
    public bool isGrounded;
    private bool canMove;
    private bool isZoom;

    private Camera playerCamera;
    
    public GameObject camPrincipal;
    public GameObject camAiming;
    public GameObject postcamPrincipal;
    public GameObject postcamAiming;
    private float m_VerticalRotation = 0;

    public GameObject gunPrincipal;
    public GameObject swordPrincipal;
    public GameObject swordInBack;
    public GameObject gunInLeg;

    private GameObject clonBulletGun;

    public ParticleSystem explosionBullet;

    private PlayerManager playerManager;

    public UIManager uiManager;

    //public AudioClip footstepSound;
    public GameObject soundForest;
    public GameObject soundWater;
    //public GameObject stepsSound;
    //public GameObject soundCity;
    //private AudioSource audioSource;

    private void Start()
    {
        //explosionBullet.Stop();
        camAiming.SetActive(false);
        camPrincipal.SetActive(true);
        postcamAiming.SetActive(false);
        postcamPrincipal.SetActive(true);
        gunPrincipal.SetActive(false);
        swordPrincipal.SetActive(false);
        swordInBack.SetActive(true);
        gunInLeg.SetActive(true);
        playerCamera = GetComponentInChildren<Camera>();
        playerManager= GetComponent<PlayerManager>();
        explosionBullet = GetComponentInChildren<ParticleSystem>();
        uiManager = GameObject.FindObjectOfType<UIManager>();
        ResumeGame();
        isZoom = false;
        //playerManager.UpdateAmmoTextGun();
        //audioSource = GetComponent<AudioSource>();
        //audioSource.clip = footstepSound;
        soundForest = (GameObject)GameObject.FindGameObjectWithTag("SoundForest");
        soundWater = (GameObject)GameObject.FindGameObjectWithTag("SoundWater");
        //stepsSound = (GameObject)GameObject.FindGameObjectWithTag("StepsSound");
        

        //soundCity = (GameObject)GameObject.FindGameObjectWithTag("SoundCity");
    }

    private void MouseRotate ()
    {
        if (canMove && !playerManager.IsDead)
        {
            float rotateHorizontal = Input.GetAxis("Mouse X") * m_MouseSensitivity;
            float rotateVertical = Input.GetAxis("Mouse Y") * m_MouseSensitivity;

            transform.Rotate(0, rotateHorizontal, 0);
            m_VerticalRotation -= rotateVertical;
            m_VerticalRotation = Mathf.Clamp(m_VerticalRotation, -90f, 90f);
            playerCamera.transform.localRotation = Quaternion.Euler(m_VerticalRotation, 0, 0);
        }
        
    }

    private void AimingOn()
    {
        camPrincipal.SetActive(false);
        camAiming.SetActive(true);
        postcamPrincipal.SetActive(false);
        postcamAiming.SetActive(true);
    }

    private void AimingOff()
    {
        camPrincipal.SetActive(true);
        camAiming.SetActive(false);
        postcamPrincipal.SetActive(true);
        postcamAiming.SetActive(false);
    }

    public void ResumeGame()
    {
        Time.timeScale = 1;
        uiManager.NoGamePause();
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        canMove = true;
    }

    public void NoPause()
    {
        playerManager.IsPaused = false;
    }

    void Explode()
    {
        ParticleSystem explosionBullet = GetComponentInChildren<ParticleSystem>();
        explosionBullet.Play();
        
    }

    
    private void Update()
    {
        isGrounded = Physics.Raycast(transform.position, Vector3.down, 1f);

        

        if (Input.GetButtonDown("Jump") /*&& isGrounded*/)
        {
            jump = true;
            Debug.Log("Estoy saltando");
            
        }

        if (Input.GetMouseButtonDown(0) && playerManager.AmmoGun > 0)
        {
            
            if (!playerManager.IsDead && canMove)
            {
                Debug.Log("He disparado");
                swordInBack.SetActive(true);
                swordPrincipal.SetActive(false);
                gunInLeg.SetActive(false);
                gunPrincipal.SetActive(true);
                //fireSound.Play();
                Explode();
                playerManager.IsPaused = true;
               if(!isZoom)
                {
                    this.gameObject.GetComponent<Animator>().SetTrigger("Shooting");
                }
                
                clonBulletGun = GameObject.Instantiate(playerManager.bulletGun, playerManager.creatorBulletGun.gameObject.transform.position, Quaternion.Euler(0, 180, 0));
                clonBulletGun.GetComponent<Rigidbody>().velocity = playerManager.creatorBulletGun.transform.forward * playerManager.forceGun;
                Destroy(clonBulletGun, 3f);
               
                playerManager.AmmoGun--; // Resta una munici�n
                playerManager.UpdateAmmoTextGun(); // Actualiza el texto de municiones
            }

            

        }

        if (Input.GetMouseButtonUp(0))
        {
            Invoke("NoPause", 2f);
        }

        if (Input.GetMouseButtonDown(1) && playerManager.swordSolid > 0)
        {
            Debug.Log("Ataco con la espada");
            playerManager.IsPaused = true;
            this.gameObject.GetComponent<Animator>().SetTrigger("Shooting");
            swordInBack.SetActive(false);
            swordPrincipal.SetActive(true);
            gunPrincipal.SetActive(false);
            gunInLeg.SetActive(true);
            Explode();
            playerManager.swordSolid--; // Desgasta
            playerManager.UpdateSwordText();


        }
       
        
        if (Input.GetMouseButtonUp(1))
        {
            Invoke("NoPause", 2f);
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            AimingOn();
            playerManager.IsPaused = true;
            uiManager.pointViewImageOne.SetActive(true);
            isZoom = true;
            
        }

        if (Input.GetKeyUp(KeyCode.Q))
        {
            AimingOff();
            playerManager.IsPaused = false;
            uiManager.pointViewImageOne.SetActive(false);
            isZoom= false;
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (Time.timeScale == 1)
            {
                Debug.Log("Estoy en pausa");
                Time.timeScale = 0;
                uiManager.GamePause();
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
                canMove = false;
            }
            else
            {
                Debug.Log("Vuelvo al play");
                ResumeGame();
            }
        }
        MouseRotate();
    }

    private void FixedUpdate()
    {
        if(!playerManager.IsDead && !playerManager.IsPaused && canMove)
        {
            float horizontalInput = Input.GetAxis("Horizontal");
            float verticalInput = Input.GetAxis("Vertical");
            bool isRunning = Input.GetKey(KeyCode.LeftShift);
            

            Vector3 moveDir = new Vector3(horizontalInput, 0f, verticalInput).normalized;
            float speed = isWalking ? walkSpeed : (isRunning ? runSpeed : walkSpeed);
            transform.Translate(moveDir * speed * Time.fixedDeltaTime);

            if(isRunning && (horizontalInput != 0.0f || verticalInput != 0.0f))
            {
                this.gameObject.transform.GetComponent<Animator>().SetInteger("StatePlayer", 2);
                soundWater.GetComponent<AudioSource>().mute = false;
                Debug.Log("Estoy corriendo");
                //soundSteps.SetActive(true);
                //audioSource.Play();
            }
            if (!isRunning && (horizontalInput != 0.0f || verticalInput != 0.0f))
            {
                this.gameObject.transform.GetComponent<Animator>().SetInteger("StatePlayer", 1);

                Debug.Log("Estoy caminando");
                //soundSteps.SetActive(true);
                //audioSource.Play();
            }
            else if(verticalInput == 0.0f && verticalInput == 0.0f)
            {
                this.gameObject.transform.GetComponent<Animator>().SetInteger("StatePlayer", 0);
                soundWater.GetComponent<AudioSource>().mute = true;
                //soundSteps.SetActive(false);
                //audioSource.Stop();
            }


            if (/*isGrounded*/canMove)
            {
                moveDir.y = -stickToGroundForce;

                if (jump)
                {
                    moveDir.y = jumpSpeed;
                    jump = false;
                    this.gameObject.transform.GetComponent<Animator>().SetInteger("StatePlayer", 3);
                }
            }
            else
            {
                moveDir += Physics.gravity * gravittyMultiplier * Time.fixedDeltaTime;
            }



            GetComponent<Rigidbody>().velocity = moveDir;
        }
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("EnemyArm"))
        {
            playerManager.TakeDamage(10f);
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Heal"))
        {
            playerManager.Heal(15);
            Debug.Log("He recuperado energia");
            Destroy(other.gameObject);
            this.gameObject.GetComponent<Animator>().SetTrigger("Grabbing");
        }
        if (other.CompareTag("Ammo"))
        {
            playerManager.ReloadedAmmo(10);
            Debug.Log("He recargado municiones");
            Destroy(other.gameObject);
            this.gameObject.GetComponent<Animator>().SetTrigger("Grabbing");
        }
        if(other.CompareTag("Finish"))
        {
            playerManager.Win();
            Debug.Log("He ganado");
            Destroy(other.gameObject);
            canMove = false;
            Cursor.visible = true;
        }
        if (other.CompareTag("Water"))
        {
            Debug.Log("Me ahogo");
            //Invoke("playerManager.Die", 1f);
            playerManager.Die();
            
        }
        if (other.CompareTag("SoundOff"))
        {
            Debug.Log("Sonidos");
            //Invoke("playerManager.Die", 1f);
            soundWater.GetComponent<AudioSource>().mute = true;
            soundForest.GetComponent<AudioSource>().mute = true;
        }

        if (other.CompareTag("SoundOn"))
        {
            Debug.Log("Sonidos");
            //Invoke("playerManager.Die", 1f);
            soundWater.GetComponent<AudioSource>().mute = false;
            soundForest.GetComponent<AudioSource>().mute = false;
        }
    }

    /*public void OnHitByPlayer()
    {
        playerManager.TakeDamage(50);
    }*/

    

}

