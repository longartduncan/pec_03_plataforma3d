using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyManager : MonoBehaviour
{
    [HideInInspector] public PatrolState patrolState;
    [HideInInspector] public AlertState alertState;
    [HideInInspector] public AttackState attackState;
    [HideInInspector] public IEnemyState currentState;

    [HideInInspector] public NavMeshAgent navMeshAgent;

    
    public float health = 90;
    
    public float damageForce = 10f;
    public float rotationTime = 7.0f;
    public float shootHeight = 0.5f;
    //public Transform[] wayPoints;

    public GameObject healCreate;
    public GameObject enemyCreate;
    public GameObject enemyOnePointOrigin;
    public ParticleSystem explosionDead;

    public bool isDead;
    public bool isWin;
    
    //public HealthManager healthManager;

    private void Start()
    { 
        //Dos estados de AI
        patrolState = new PatrolState(this);
        alertState = new AlertState(this);
        attackState = new AttackState(this);

        currentState = patrolState;

        navMeshAgent = GetComponent<NavMeshAgent>();
        explosionDead= GetComponentInChildren<ParticleSystem>();
        //healthManager = GetComponent<HealthManager>();
        isDead = false;
        isWin = false;
        
        


    }
    void Explode()
    {
        ParticleSystem explosionDead = GetComponentInChildren<ParticleSystem>();
        explosionDead.Play();
        //Destroy(this.gameObject, explosionBullet.main.duration);
    }
    private void Update()
    {
        currentState.UpdateState();
        if (isWin)
        {
            this.gameObject.transform.GetComponent<Animator>().SetInteger("StateEnemy", 0);
        }

        if (health < 0) Die();
    }

    public void Hit(float damage)
    {
        health -= damage;
        currentState.Impact();
        Debug.Log("Enemy hitted: " + health);
    }

    private void OnTriggerEnter(Collider col)
    {
        currentState.OnTriggerEnter(col);

    }

    private void OnTriggerStay(Collider col)
    {
        currentState.OnTriggerStay(col);

    }

    private void OnTriggerExit(Collider col)
    {
        currentState.OnTriggerExit(col);

    }

    public void Stop()
    {
        navMeshAgent.isStopped = true;
        navMeshAgent.velocity = Vector3.zero;
        
    }

    public void Resume()
    {
        navMeshAgent.isStopped = false;
        
    }


    public void TakeDamage(float amount)
    {
        health -= amount;
        if (health <= 0)
        {
            Die();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bullet"))
        {
            
            Vector3 enemyPosition = collision.gameObject.transform.position;
            TakeDamage(30f);
            Debug.Log("Le he dado al enemigo");
            if (health <= 0)
            {                           
                Instantiate(healCreate, enemyPosition, Quaternion.identity);
                Instantiate(enemyCreate, enemyOnePointOrigin.gameObject.transform.position, Quaternion.identity);
                
            }
            
            
        }
    }

    private void Die()
    {
        Debug.Log("El enemigo esta destruido");
        isDead = true;
        Explode();
        this.gameObject.GetComponent<Animator>().SetTrigger("Dying");
        Invoke("OnDestroy", 5f);
        
    }

    


    private void OnDestroy()
    {
        Destroy(this.gameObject);
    }

    
    

}
